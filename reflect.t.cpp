#include <mirror/mirror.hpp>

#include <catch/catch.hpp>

#include <string>
#include <iostream>

class MyClass
{
public:
    int foo;
    std::string cat;
};

MIRRORPP_REFLECT(MyClass, (foo)(cat));

template <typename T>
int foo(mirror::tail_member<T>)
{
    return 12;
}

template <typename Info>
int foo(Info info)
{
    return foo(typename Info::next{});
}

// A simple recursive type printer:
template <typename T, typename Info>
void print_one(std::ostream& os, const T& t, Info)
{
    // Print out the info
    os << Info::name() << ' ' << Info::ref(t) << '\n';
    // Print the next member:
    print_one(os, t, typename Info::next{});
}

template <typename T>
void print_one(std::ostream& os, const T&, mirror::tail_member<T>)
{
    // Reached the end of recursion
}

template <typename T>
void print(std::ostream& os, const T& item)
{
    print_one(os, item, typename mirror::reflect<T>::first{});
}

TEST_CASE("Type traits")
{
    static_assert(mirror::detail::basic_type_info<MyClass>::num_members == 2,
                  "Bad");
    static_assert(mirror::size_of<MyClass> == 2, "Bad");
    mirror::reflect<MyClass> r;
    static_assert(mirror::is_reflected<MyClass>::value, "Bad");
    static_assert(mirror::reflect<MyClass>::nth_member<0>::type() == 0, "Bad");
    static_assert(!mirror::is_reflected<int>::value, "Bad");
    static_assert(mirror::nth_member_of<MyClass, 0>::type{} == 0, "Bad");
    CHECK((mirror::nth_name<MyClass, 0>) == std::string{ "foo" });
    static_assert(std::is_same<mirror::nth_type<MyClass, 1>, std::string>::value, "Bad");
    foo(mirror::reflect<MyClass>::nth_member<0>{});
    print(std::cout, MyClass());
}
