cmake_minimum_required(VERSION 3.0.0)
project(mirrorpp VERSION 0.0.0 LANGUAGES CXX)

set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)

include(CTest)
enable_testing()

find_package(Pew QUIET)
if(Pew_FOUND)
    find_package(boost REQUIRED)
else()
    find_package(Boost REQUIRED)
endif()
find_package(catch QUIET)

set(headers
    mirror/mirror.hpp
    mirror/macro.hpp
    )
add_library(reflect
    dummy.cpp
    ${headers}
    )
set(PEW_PLATFORM_ID all)
set(PEW_INCLUDE_DESTINATION_SUFFIX mirror)
set_property(TARGET reflect PROPERTY PUBLIC_HEADER ${headers})

if(Pew_FOUND)
    target_link_libraries(reflect PUBLIC boost::preprocessor)
else()
    target_include_directories(reflect PUBLIC "${Boost_INCLUDE_DIR}")
endif()

target_include_directories(reflect PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>)
target_compile_features(reflect PUBLIC cxx_static_assert cxx_alias_templates cxx_decltype)
set_property(TARGET reflect PROPERTY CXX_EXTENSIONS NO)

target_compile_definitions(reflect PUBLIC
    $<$<COMPILE_FEATURES:cxx_variable_templates>:MIRRORPP_HAVE_VARIABLE_TEMPLATES>
    )

if(TARGET catch::main)
    include(tests.cmake)
endif()

if(Pew_FOUND)
    pew_export_project()
endif()
