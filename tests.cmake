set(test_names
    reflect
    )

foreach(name IN LISTS test_names)
    add_executable(test.${name} ${name}.t.cpp)
    target_link_libraries(test.${name} PRIVATE reflect)
    catch_add_tests(${name} test.${name})
endforeach()
