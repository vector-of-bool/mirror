# Mirror++
## The Smallest C++ Reflection Library in the Universe

Mirror++ is a small header-only library to do rudimentary reflection on C++
``class`` and ``struct`` types. It does not require RTTI, and is completely
type-safe. Usage is simple:

``` c++

#include <string>

// Include the Mirror++ reflection header
#include <mirror/reflect.hpp>

struct Car
{
    int numTires;
    std::string make;
    std::string model;
    std::string titleOwner;
};

// Expose a set of traits to accessing our struct
MIRROR_REFLECT(Car,
    (numTires)
    (make)
    (model)
    (titleOwner)
    );

void foo()
{
    // Everything is exposed via the mirror::reflect template
    using refl = mirror::reflect<Car>;
    // We can use 'refl' to get information about the car class


    // Get the number of members on the class:
    mirror::reflect<Car>::size;

    // Access information about the Nth member:
    using first_member = mirror::nth_member<Car, 0>;
    // (We can also get at it using mirror::reflect<T>::first)
    first_member::type value;  // <-- int
    first_member::name(); // <-- "numTires"

    // Given an instance of a reflected type, we can get access to the nth
    // member using the 'ref' static method:
    Car c;
    auto something = first_member:ref(c);
    // We can even assign (unless given a const object):
    first_member::ref(c) = something;

    // We can shift to the second using nth_member<1>, or:
    using second_member = first_member::next;

}

// If the next member does not exist, we have a tag type called
// mirror::tail_member<T>. This is useful for recursively iterating over the
// members on a class. See below:

// A simple recursive object printing function:

template <typename T, typename Info>
void print_member(std::ostream& os, const T& t, Info)
{
    // Print out the info
    os << Info::name() << ": " << Info::ref(t) << '\n';
    // Print the next member:
    print_member(os, t, typename Info::next{});
}

template <typename T>
void print_member(std::ostream& os, const T&, mirror::tail_member<T>)
{
    // When Info::next yields type mirror::tail_member<T>, we'll end up in this
    // function, and we are done iterating
}

template <typename T>
void print(std::ostream& os, const T& item)
{
    print_one(os, item, typename mirror::reflect<T>::first{});
}

```
